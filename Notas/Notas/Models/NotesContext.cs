﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Notas.Models
{
    public class NotesContext : DbContext
    {
        //Herede de la clase principal que seria DbContext
        public NotesContext() : base("DefaultConnection")
        {

        }

        //Para cerrar la conexion
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public DbSet<User> Users { get; set; }
    }
}